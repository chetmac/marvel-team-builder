import React from 'react';
import {useMarvel} from '../Hooks/useMarvel.js';

export const ViewHistory = (props) => {

	const {
		actionHistory,
		undo
	} = useMarvel();

	return (
		<div>
			<button onClick={ e => actionHistory.length ? undo() : null } >Undo</button>
			{
				actionHistory.map( (h,index) => (
					<div className="card" key={`action-${index}`}>
						{h.action}: {h.character.name} 
					</div>
				))
			}
		</div>
	)

}