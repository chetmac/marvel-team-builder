import React from 'react';
import {ProvideMarvel} from './Hooks/useMarvel.js';
import {BrowseCharacters} from './Components/BrowseCharacters.js'
import {ManageTeam} from './Components/ManageTeam.js'
import {ViewHistory} from './Components/ViewHistory.js'

import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import { ToastContainer } from 'react-toastify';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

      <ProvideMarvel>
        <Tabs defaultActiveKey="browse" id="uncontrolled-tab-example" variant="pills">
          <Tab eventKey="browse" title="Browse">
              <BrowseCharacters />
          </Tab>
          <Tab eventKey="manage" title="Manage">
            <ManageTeam />
          </Tab>
          <Tab eventKey="history" title="History">
            <ViewHistory />
          </Tab>
        </Tabs>

        
        
      </ProvideMarvel>
    </div>
  );
}

export default App;
