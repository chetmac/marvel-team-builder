import React, { useState, useEffect, useContext, createContext, useCallback } from "react";
import axios from 'axios';
import {useLocalStorage} from './useLocalStorage.js';
import eventBus from "../utils/eventBus.js";

import { toast } from 'react-toastify';

const marvelContext = createContext();

export function ProvideMarvel({ children }) {
    const marvel = useProvideMarvel();
    return <marvelContext.Provider value={marvel}>{children}</marvelContext.Provider>;
}

export const useMarvel = () => {
    return useContext(marvelContext);
};

function useProvideMarvel() {

    const baseUrl = `https://gateway.marvel.com:443/v1/public/characters?apikey=${process.env.REACT_APP_MARVEL_API_KEY}`;
    
    const [team,setTeam] = useLocalStorage("marvel-team",[]);

    const [query,setQuery] = useLocalStorage("marvel-query", '');
    const [offset,setOffset] = useLocalStorage("marvel-offset", 0);
    const [limit,setLimit] = useState(20);

    const [pageResults,setPageResults] = useState([]);

    const [actionHistory,setActionHistory] = useLocalStorage("marvel-history",[]);

    const [pageMeta,setPageMeta] = useState({
        count: 0,
        total: 0,
        limit,
        query,
        offset
    });

    const addCharacter = (character) => {
        setActionHistory( _h => [{action:"add", character},..._h]);
        // Don't allow adding twice under any circumstances.
        if ( ! team.find( c => c.id === character.id) ){
            
            setTeam( _team => {
                if ( character.teamIndex !== null ){
                    // splice to index
                    let newTeam = [..._team];
                    newTeam.splice(character.teamIndex, 0, character);
                    return newTeam;
                } else {
                    // append to end
                    const teamIndex = _team.length;
                    return [..._team, {...character, teamIndex}];
                }

            });

        }
    }

    const removeCharacter = (character) => {
        console.log("remove character ",character.name,team);
        setActionHistory( _h => [{action:"remove", character},..._h]);
        setTeam( _team => _team.filter( _character => _character.id !== character.id ));
    }

    const safeRemoveCharacter = (character) => {
        setActionHistory( _h => [{action:"remove", character},..._h]);
        removeCharacter(character);
    }

    const undo = () => {
        setActionHistory( _h => {
            const item = _h.shift();
            const fn = item.action === "add" ? removeCharacter : addCharacter;
            fn(item.character);
            return _h;
        });
    }

    const setFilter = (filter) => {
        if ( filter !== query ){
            setOffset(0);
            setQuery(filter);
        }
    }

    const prevPage = () => {
        if ( pageMeta.prevPage )
            gotoPage(pageMeta.prevPage);
    }

    const nextPage = () => {
        if ( pageMeta.nextPage )
            gotoPage(pageMeta.nextPage);
    }

    const gotoPage = (page) => {
        setOffset( (page-1) * limit );
    }

    const fetchCharacters = () => {

        const nameStartsWith = query && query !== '' ? `&nameStartsWith=${query}` : '';
        const url = `${baseUrl}&limit=${limit}&offset=${offset}${nameStartsWith}`;

        axios.get(url)
        .then( res => {

            // Let's tidy up our results
            const {limit, count, total, offset, results} = res.data.data;

            let totalPages = 0;
            let currentPage = 0;
            let prevPage = null;
            let nextPage = null;

            if ( total ){
                totalPages = Math.ceil(total / limit);
                currentPage = offset+1 > limit ? Math.ceil((offset+1) / limit) : 1;
                prevPage = currentPage > 1 ? currentPage - 1 : null;
                nextPage = currentPage < totalPages ? currentPage + 1 : null;                
            }

            setPageMeta({
                limit, count, total, offset,
                totalPages, currentPage, prevPage, nextPage
            });

            const characterIndex = team.reduce( (obj, m, index) => ({...obj, [m.id]:index}), {});

            setPageResults(results.map( m => {
                const teamIndex = characterIndex.hasOwnProperty(m.id) ? characterIndex[m.id] : null;
                return {...m, teamIndex}
            }));

        })
        .catch( error => {
            console.log(error);
        });

    }

    useEffect( () => {
        fetchCharacters();
    },[query, offset, limit])

    // useEffect( () => {

    //     if ( removedCharacter ){

    //         toast.dismiss();
    //         toast(
    //             <span onClick={ e => addCharacter(removedCharacter) }>Removed {removedCharacter.name}. <span style={{color:"blue"}}>Undo?</span></span>
    //         );

    //     }

    // }, [ removedCharacter ])

    return {
        actionHistory,
        undo,
        setLimit,
        setFilter,
        filter: query,
        pageResults,
        pageMeta,
        prevPage,
        nextPage,
        gotoPage,
        addCharacter,
        removeCharacter,
        safeRemoveCharacter,
        team
    }

}