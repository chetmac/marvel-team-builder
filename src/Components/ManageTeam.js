import React, {useEffect, useCallback} from 'react';
import {useMarvel} from '../Hooks/useMarvel.js';
import {Character} from './Character';

import { toast } from 'react-toastify';

import eventBus from "../utils/eventBus.js";

import 'react-toastify/dist/ReactToastify.css';

export const ManageTeam = () => {

	const {
		team,
		addCharacter,
		safeRemoveCharacter,
	} = useMarvel();

	return (
        <div>
        	{
        		team.map( c => 
					<Character  key={c.id} 
								character={c} 
								addCharacter={ e => addCharacter(c) }
								removeCharacter={ e => { safeRemoveCharacter(c) } }
        						isTeamMember={true}
        			/> 
        		)
        	}
        </div>
	);

}