import React, {useRef,useEffect,useMemo} from 'react';
import {useMarvel} from '../Hooks/useMarvel.js';
import {Character} from './Character';

export const BrowseCharacters = () => {

	const {
		filter, 
		setFilter, 
		addCharacter,
		removeCharacter, 
		nextPage, 
		prevPage, 
		pageMeta, 
		pageResults,
		team
	} = useMarvel();

	const filterInput = useRef();

	const teamIds = useMemo( () => team.map( c => c.id ), [team] );

	const applyFilter = (e) => {
		e.preventDefault();
		setFilter(filterInput.current.value);
	}

	useEffect( () => {
		if ( filterInput.current ){
			filterInput.current.value = filter;
		}
	}, [filter]);

	return (
		<div>
			<div className="filter-characters">
				<form onSubmit={ applyFilter }>
					<input type="text" ref={filterInput} />
					<button type="submit">Filter</button>
					{ filter !== '' && <button type="button" onClick={ e => setFilter('') }>Clear</button> }
				</form>
			</div>
			{ Pagination({pageMeta, prevPage, nextPage}) }
			<div className="character-list">
			{
				pageResults.map( (c,index) => 
					<Character  key={c.id} 
								character={c} 
								addCharacter={ e => addCharacter(c) }
								removeCharacter={ e => removeCharacter(c) }
								isTeamMember={ teamIds.length && teamIds.includes(c.id) }
					/>
				)
			}
			</div>
			{ Pagination({pageMeta, prevPage, nextPage}) }
		</div>
	);

}

const Pagination = ({pageMeta,prevPage,nextPage,...props}) => {
	return (
		<div className="">
			<div className="">Page {pageMeta.currentPage} of {pageMeta.totalPages}</div>
			<div className="">Viewing {pageMeta.count} of {pageMeta.total} Results</div>
			<div className="">
				<button type="button" className="btn btn-sm btn-primary mr-3" onClick={ e => prevPage() } disabled={ ! pageMeta.prevPage }>Prev</button>  
				<button type="button" className="btn btn-sm btn-primary" onClick={ e => nextPage() } disabled={ ! pageMeta.nextPage }>Next</button>
			</div>
		</div>
	);
}