import React, {useEffect} from 'react';

import {useMarvel} from '../Hooks/useMarvel.js';

import Container from 'react-bootstrap/Container';

// Known Bug with Transitions:
// https://github.com/react-bootstrap/react-bootstrap/issues/5075
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import {BrowseCharacters} from './BrowseCharacters';
import {ManageTeam} from './ManageTeam';

export const TeamBuilder = (props) => {

	return (
        <Container>

	        <Tabs defaultActiveKey="browse" id="marvel-nav" variant="pills">
	          <Tab eventKey="browse" title="Browse">
	          	<BrowseCharacters />
	          </Tab>
	          <Tab eventKey="view" title="View">
	          	<ManageTeam />
	          </Tab>
	        </Tabs>

        </Container>
	);

}