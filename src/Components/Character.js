import React from 'react';
import "./Character.css"

export const Character = ({character, removeCharacter, addCharacter, ...props}) => {

	const imageSelector = "landscape_xlarge";
	const imageSource = `${character.thumbnail.path}/${imageSelector}.${character.thumbnail.extension}`;

	return (
		<div className="card mb-5">
			<div className="row mb-2 mt-2">
				<div className="col-md-3">
					<div className="mb-2"><img src={imageSource} style={{width:"120px"}} alt={character.name}/></div>
					{
					props.isTeamMember === true
					? <button type="button" className="btn btn-sm btn-primary" onClick={ removeCharacter } >Remove</button>	
					: <button type="button" className="btn btn-sm btn-secondary" onClick={ addCharacter } >Add</button>
					}
				</div>
				<div className="col-md-9">
					<h2>{character.name}</h2>
					<p>{character.description}</p>
				</div>
			</div>
		</div>
	);

}