// https://www.pluralsight.com/guides/how-to-communicate-between-independent-components-in-reactjs

const eventBus = {
  on(event, callback) {
  	// console.log("CB: Adding to "+event,callback);
    // window.addEventListener(event, (e) => callback(e.detail));
    window.addEventListener(event, callback);
  },
  dispatch(event, data) {
    window.dispatchEvent(new CustomEvent(event, { detail: data }));
  },
  remove(event, callback) {
  	// console.log("CB: Attempting to remove from "+event,callback);
    window.removeEventListener(event, callback);
  },
};

export default eventBus;